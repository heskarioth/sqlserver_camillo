create procedure trade_3_days

as

SELECT 
	c1.[date]
	,((c1.[High] - c1.[Low]) *c1. volume) AS Trading_range_volume
	,c1.[time] AS time_of_max_price
FROM challenge_2 c1
RIGHT JOIN
	( 
		SELECT c.[date] AS [datefin] 
			,MAX (c.[high]) AS [high]
		FROM challenge_2 c
		WHERE c.[Date] IN (
		SELECT d.[Date]
		FROM
		(
		SELECT TOP 3 
						[Date]
						,MAX(([High] - [Low]) * volume) AS Trading_range_volume
					FROM [challenges].[dbo].[challenge_2]
					GROUP BY [Date]
					ORDER BY 2 DESC

		) d) 
		GROUP BY c.[date]
	) f ON f.datefin=c1.[Date] AND f.[high]=c1.[high]

go


exec trade_3_days	