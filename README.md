Hello,

Let me explain how the two queries work / assumptions made in the 
process.

Challenge 1:
-  This is a stored procedure, taking two inputs (inputhour and 
inputdate). If you want to check the hours between 5 and 10am, you would 
give a parameter of 5 (no zeros required). 
- The input for the date is an actual integer so you have to follow the 
format: YYYYMMDD (e.g. 20101011), no spacing or special simbols in 
between.

Challenge 2:
- The trade volume is calculated as (high-low) * volum -> this is used 
to calculate the 3 days which had the highest volume of trades
- From there, the query then takes the highest 'High' for each of those 
days
- Finally, the highest 'High', the actual 'date' are used to join with 
the original table and retreive the remaining information we are looking 
for (in this case max time).
