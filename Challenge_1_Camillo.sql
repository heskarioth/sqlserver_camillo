
--declare @inputhour varchar(20) = '13'
--declare @inputdate varchar(10) = '20101011'

create procedure vwap 
 @inputhour varchar(20), @inputdate varchar(10)

as
 


  SELECT 
	SUM([close] *vol)/SUM(vol) AS VWAP
	,right(left([date],8),2)  +'/'+ right(left([date],6),2) + '/'+ left([date],4) as [Date]
	--,FORMAT(DATEADD(hh,@inputhour,'00:00'), 'hh:mm tt') as 'Start'
	, 'Start: (' + case when convert(int,@inputhour)<10 then '0' + @inputhour + ':00)' else @inputhour + ':00)' end as 'Start'
	, 'End: (' + case when convert(int,@inputhour)+5 >23 then '00:00)' else convert(varchar(50),convert(int,@inputhour)+5)+':00)' end as 'End'
	
  FROM challenge_1
  WHERE left([date],8) = @inputdate and convert(int,left(right([date],4),2)) between convert(int,@inputhour) and (convert(int,left(right([date],4),2)))+5
  group by [date]

GO

 exec vwap @inputhour = '13', @inputdate = '20101011'

